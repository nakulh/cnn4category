4 category CNN for simple shapes
================================

Running Training
--------------------------------

* Clone this repository.

* Install dependencies like tensorflow etc.

* Put training images in respective folders, category wise, name them `0, 1, 2, 3`.

* Put test images in one folder `demoTest`.

* Run `python squeeze_net.py`.

Note that you can change how often training report is generated and saved in .csv file format.
It saves checkpoint in both .npz and .pb format


Tunable Hyperparameters
--------------------------------

* Number of fire modules.

* Number of 1x1 filters in fire modules

* Learning Rate

* Batch Size

