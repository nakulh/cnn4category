import os
import cv2
import glob
import random
import numpy as np
import tensorflow as tf
import tensorflow_hub as hub
from tensorflow.python.framework.graph_util import convert_variables_to_constants
import csv

train_steps = 10000
learn_rate = 0.001
num_classes = 4
batch_size = 32
train_report = 50
eval_report = 100
reportFile = "report.csv"
testAccFile = "testAcc.csv"
checkpoint_steps = 200
#retrain = False
latest_weight = 0
#reportFile= 'report.csv'
#reportFile2='report.csv'

def weight(shape, name=None):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)


def bias(shape, name=None):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)


def conv(x, W, stride=(1, 1), padding='SAME'):
    return tf.nn.conv2d(x, W, strides=[1, stride[0], stride[1], 1], padding=padding)


def max_pool(x, ksize=(3, 3), stride=(2, 2)):
    return tf.nn.max_pool(x, ksize=[1, ksize[0], ksize[1], 1], strides=[1, stride[0], stride[1], 1], padding='SAME')


def avg_pool(x, ksize=(8, 8), stride=(1, 1)):
    return tf.nn.avg_pool(x, ksize=[1, ksize[0], ksize[1], 1], strides=[1, stride[0], stride[1], 1], padding='VALID')


def relu(input_layer):
    return tf.nn.relu(input_layer)

def one_hot(y):
    return np.eye(num_classes, 1, -1*y)

def unzip(b):
    xs,ys = zip(*b)
    xs = np.array(xs)
    ys = np.array(ys)
    return xs, ys

def unzipTrain(*b):
    xs = []
    ys = []
    for x in range(len(b)):
        txs, tys = zip(*b[x])
        xs.extend(txs)
        ys.extend(tys)
    xs = np.array(xs)
    ys = np.array(ys)
    return xs, ys

def train_prototype(path, contrib):
    for fname in random.sample((glob.glob(path)),contrib):
        img = cv2.imread(fname, 0)
        img = cv2.resize(img, (128,128))/255.
        code = int(fname.split('\\')[-1].split('_')[0])
        label = one_hot(code)
        yield img, label


def read_train_1():
    return train_prototype('0/*', batch_size//num_classes)
        

def read_train_2():
    return train_prototype('1/*', batch_size//num_classes)


def read_train_3():
    return train_prototype('2/*', batch_size//num_classes)


def read_train_4():
    return train_prototype('3/*', batch_size//num_classes)


def read_test():
    for fname in glob.glob('demoTest/*'):
        img = cv2.imread(fname,0)
        img = cv2.resize(img,(128,128))/255.
        code = int(fname.split('\\')[-1].split('_')[0])
        label = one_hot(code)
        yield img, label
        
                
        
def bn(inputs, training, epsilon=1e-7):
    decay=0.999
    gamma   = tf.Variable(tf.constant(1.0, shape=[int(inputs.get_shape()[-1])]), name='gamma', trainable=True)
    beta    = tf.Variable(tf.constant(0.0, shape=[int(inputs.get_shape()[-1])]), name='beta' , trainable=True)
    pop_mean= tf.Variable(tf.constant(0.0, shape=[int(inputs.get_shape()[-1])]), name='pop_mean' , trainable=False)
    pop_var = tf.Variable(tf.constant(1.0, shape=[int(inputs.get_shape()[-1])]), name='pop_var' , trainable=False)
    if training:
        batch_mean, batch_var = tf.nn.moments(inputs, [0,1,2])
        train_mean = tf.assign(pop_mean, pop_mean*decay + batch_mean*(1-decay))
        train_var  = tf.assign(pop_var , pop_var*decay  + batch_var*(1-decay)); 
        with tf.control_dependencies([train_mean, train_var]):
            return tf.nn.batch_normalization(inputs, batch_mean, batch_var, beta, gamma, epsilon), gamma, beta, pop_mean, pop_var
    else:
        return tf.nn.batch_normalization(inputs, pop_mean, pop_var, beta, gamma, epsilon), gamma, beta, pop_mean, pop_var


#####################  Fire module for training mode comprising of squeeze and expand layers  ###############################
def fire(input_layer, s1x1, e1x1, e3x3, bypass = False):
    s1x1 = int(s1x1)
    w_s1x1 = weight([1,1,int(input_layer.get_shape()[-1]),s1x1])
    w_e1x1 = weight([1,1,s1x1,e1x1])
    w_e3x3 = weight([3,3,s1x1,e3x3])
    fs = conv(input_layer, w_s1x1)
    fs_bn, gamma_f1, beta_f1, pop_mean_f1, pop_var_f1 = bn(fs,True)
    fs_r = relu(fs)
    fe_1 = conv(fs_r, w_e1x1)
    fe_3 = conv(fs_r, w_e3x3)
    fe_4, gamma_f2, beta_f2, pop_mean_f2, pop_var_f2 = bn(fe_1,True)
    fe_5, gamma_f3, beta_f3, pop_mean_f3, pop_var_f3 = bn(fe_3,True)
    fe = tf.concat([fe_4,fe_5], axis=3)
    if bypass == False:
        out = relu(fe)
    else:
        out = relu(tf.add(fe, input_layer))
    return w_s1x1, w_e1x1, w_e3x3, gamma_f1, beta_f1, pop_mean_f1, pop_var_f1, gamma_f2, beta_f2, pop_mean_f2, pop_var_f2, gamma_f3, beta_f3, pop_mean_f3, pop_var_f3, out
  
#####################  SqueezeNet Architecture for training mode  ###############################
def build_model():
    sq_ratio=0.125
    x = tf.placeholder(tf.float32, [None, None, None], name='input_node')
    x_expanded = tf.expand_dims(x,3)
    w1 = weight([3,3,1,64]); 
    conv1 = conv(x_expanded, w1, stride=(2,2))
    conv1_bn, gamma_1, beta_1, pop_mean_1, pop_var_1 = bn(conv1, True)
    relu1 = relu(conv1_bn)
    m_pool_1 = max_pool(relu1)
    #Fire-Stack-1
    w_s1x1_1, we1x1_1, we3x3_1, gamma_f1_1, beta_f1_1, pop_mean_f1_1, pop_var_f1_1, gamma_f2_1, beta_f2_1, pop_mean_f2_1, pop_var_f2_1, gamma_f3_1, beta_f3_1, pop_mean_f3_1, pop_var_f3_1, fire_1 = fire(m_pool_1, sq_ratio*128, 64, 64, False)
    w_s1x1_2, we1x1_2, we3x3_2, gamma_f1_2, beta_f1_2, pop_mean_f1_2, pop_var_f1_2, gamma_f2_2, beta_f2_2, pop_mean_f2_2, pop_var_f2_2, gamma_f3_2, beta_f3_2, pop_mean_f3_2, pop_var_f3_2, fire_2 = fire(fire_1,   sq_ratio*128, 64, 64, True)
    m_pool_2 = max_pool(fire_2)
    #Fire-Stack-2
    w_s1x1_3, we1x1_3, we3x3_3, gamma_f1_3, beta_f1_3, pop_mean_f1_3, pop_var_f1_3, gamma_f2_3, beta_f2_3, pop_mean_f2_3, pop_var_f2_3, gamma_f3_3, beta_f3_3, pop_mean_f3_3, pop_var_f3_3, fire_3 = fire(m_pool_2, sq_ratio*256, 128, 128, False)
    w_s1x1_4, we1x1_4, we3x3_4, gamma_f1_4, beta_f1_4, pop_mean_f1_4, pop_var_f1_4, gamma_f2_4, beta_f2_4, pop_mean_f2_4, pop_var_f2_4, gamma_f3_4, beta_f3_4, pop_mean_f3_4, pop_var_f3_4, fire_4 = fire(fire_3,   sq_ratio*256, 128, 128, True)
    m_pool_3 = max_pool(fire_4)
    #Fire-Stack-3
    w_s1x1_5, we1x1_5, we3x3_5, gamma_f1_5, beta_f1_5, pop_mean_f1_5, pop_var_f1_5, gamma_f2_5, beta_f2_5, pop_mean_f2_5, pop_var_f2_5, gamma_f3_5, beta_f3_5, pop_mean_f3_5, pop_var_f3_5, fire_5 = fire(m_pool_3, sq_ratio*512,256,256, False)
    '''w_s1x1_6, we1x1_6, we3x3_6, gamma_f1_6, beta_f1_6, pop_mean_f1_6, pop_var_f1_6, gamma_f2_6, beta_f2_6, pop_mean_f2_6, pop_var_f2_6, gamma_f3_6, beta_f3_6, pop_mean_f3_6, pop_var_f3_6, fire_6 = fire(fire_5,   sq_ratio*512,256,256, True)
    m_pool_4 = max_pool(fire_6)
    #Final-Fire-Module
    w_s1x1_7, we1x1_7, we3x3_7, gamma_f1_7, beta_f1_7, pop_mean_f1_7, pop_var_f1_7, gamma_f2_7, beta_f2_7, pop_mean_f2_7, pop_var_f2_7, gamma_f3_7, beta_f3_7, pop_mean_f3_7, pop_var_f3_7, fire_7 = fire(m_pool_4, sq_ratio*1024,512,512, False)
    #Convolution-Final'''
    w2 = weight([1,1,512,num_classes]);
    conv2 = conv(fire_5, w2)
    conv2_bn, gamma_2, beta_2, pop_mean_2, pop_var_2 = bn(conv2, True)
    relu2 = relu(conv2_bn)
    final_pool = avg_pool(relu2)
    out = tf.reshape(final_pool,[-1, num_classes], name='output_node')
    hub.add_signature(inputs=x, outputs=out)
    return x,out,[w1, 
                  w_s1x1_1, we1x1_1, we3x3_1, 
                  w_s1x1_2, we1x1_2, we3x3_2, 
                  w_s1x1_3, we1x1_3, we3x3_3,
                  w_s1x1_4, we1x1_4, we3x3_4,
                  w_s1x1_5, we1x1_5, we3x3_5,
                  w2, 
                  gamma_1, beta_1, pop_mean_1, pop_var_1,
                  gamma_f1_1, beta_f1_1, pop_mean_f1_1, pop_var_f1_1, 
                  gamma_f2_1, beta_f2_1, pop_mean_f2_1, pop_var_f2_1, 
                  gamma_f3_1, beta_f3_1, pop_mean_f3_1, pop_var_f3_1, 
                  gamma_f1_2, beta_f1_2, pop_mean_f1_2, pop_var_f1_2, 
                  gamma_f2_2, beta_f2_2, pop_mean_f2_2, pop_var_f2_2, 
                  gamma_f3_2, beta_f3_2, pop_mean_f3_2, pop_var_f3_2,
                  gamma_f1_3, beta_f1_3, pop_mean_f1_3, pop_var_f1_3, 
                  gamma_f2_3, beta_f2_3, pop_mean_f2_3, pop_var_f2_3, 
                  gamma_f3_3, beta_f3_3, pop_mean_f3_3, pop_var_f3_3,
                  gamma_f1_4, beta_f1_4, pop_mean_f1_4, pop_var_f1_4,
                  gamma_f2_4, beta_f2_4, pop_mean_f2_4, pop_var_f2_4,
                  gamma_f3_4, beta_f3_4, pop_mean_f3_4, pop_var_f3_4,
                  gamma_f1_5, beta_f1_5, pop_mean_f1_5, pop_var_f1_5,
                  gamma_f2_5, beta_f2_5, pop_mean_f2_5, pop_var_f2_5,
                  gamma_f3_5, beta_f3_5, pop_mean_f3_5, pop_var_f3_5,
                  gamma_2, beta_2, pop_mean_2, pop_var_2]

#####################  Fire module for testing mode comprising of squeeze and expand layers  ###############################
def fire_f(input_layer, s1x1, e1x1, e3x3, bypass = False):
    s1x1 = int(s1x1)
    w_s1x1 = weight([1,1,int(input_layer.get_shape()[-1]),s1x1])
    w_e1x1 = weight([1,1,s1x1,e1x1])
    w_e3x3 = weight([3,3,s1x1,e3x3])
    fs = conv(input_layer, w_s1x1)
    fs_bn, gamma_f1, beta_f1, pop_mean_f1, pop_var_f1 = bn(fs,False)
    fs_r = relu(fs)
    fe_1 = conv(fs_r, w_e1x1)
    fe_3 = conv(fs_r, w_e3x3)
    fe_4, gamma_f2, beta_f2, pop_mean_f2, pop_var_f2 = bn(fe_1,False)
    fe_5, gamma_f3, beta_f3, pop_mean_f3, pop_var_f3 = bn(fe_3,False)
    fe = tf.concat([fe_4,fe_5], axis=3)
    if bypass == False:
        out = relu(fe)
    else:
        out = relu(tf.add(fe, input_layer))
    return w_s1x1, w_e1x1, w_e3x3, gamma_f1, beta_f1, pop_mean_f1, pop_var_f1, gamma_f2, beta_f2, pop_mean_f2, pop_var_f2, gamma_f3, beta_f3, pop_mean_f3, pop_var_f3, out
  

#####################  SqueezeNet Architecture for testing mode  ###############################
def build_model_f():
    sq_ratio=0.125
    x = tf.placeholder(tf.float32, [None, None, None], name='input_node')
    x_expanded = tf.expand_dims(x,3)
    w1 = weight([3,3,1,64]); 
    conv1 = conv(x_expanded, w1, stride=(2,2))
    conv1_bn, gamma_1, beta_1, pop_mean_1, pop_var_1 = bn(conv1, False)
    relu1 = relu(conv1_bn)
    m_pool_1 = max_pool(relu1)
    #Fire-Stack-1
    w_s1x1_1, we1x1_1, we3x3_1, gamma_f1_1, beta_f1_1, pop_mean_f1_1, pop_var_f1_1, gamma_f2_1, beta_f2_1, pop_mean_f2_1, pop_var_f2_1, gamma_f3_1, beta_f3_1, pop_mean_f3_1, pop_var_f3_1, fire_1 = fire_f(m_pool_1, sq_ratio*128, 64, 64, False)
    w_s1x1_2, we1x1_2, we3x3_2, gamma_f1_2, beta_f1_2, pop_mean_f1_2, pop_var_f1_2, gamma_f2_2, beta_f2_2, pop_mean_f2_2, pop_var_f2_2, gamma_f3_2, beta_f3_2, pop_mean_f3_2, pop_var_f3_2, fire_2 = fire_f(fire_1,   sq_ratio*128, 64, 64, True)
    m_pool_2 = max_pool(fire_2)
    #Fire-Stack-2
    w_s1x1_3, we1x1_3, we3x3_3, gamma_f1_3, beta_f1_3, pop_mean_f1_3, pop_var_f1_3, gamma_f2_3, beta_f2_3, pop_mean_f2_3, pop_var_f2_3, gamma_f3_3, beta_f3_3, pop_mean_f3_3, pop_var_f3_3, fire_3 = fire_f(m_pool_2, sq_ratio*256, 128, 128, False)
    w_s1x1_4, we1x1_4, we3x3_4, gamma_f1_4, beta_f1_4, pop_mean_f1_4, pop_var_f1_4, gamma_f2_4, beta_f2_4, pop_mean_f2_4, pop_var_f2_4, gamma_f3_4, beta_f3_4, pop_mean_f3_4, pop_var_f3_4, fire_4 = fire_f(fire_3,   sq_ratio*256, 128, 128, True)
    m_pool_3 = max_pool(fire_4)
    #Fire-Stack-3
    w_s1x1_5, we1x1_5, we3x3_5, gamma_f1_5, beta_f1_5, pop_mean_f1_5, pop_var_f1_5, gamma_f2_5, beta_f2_5, pop_mean_f2_5, pop_var_f2_5, gamma_f3_5, beta_f3_5, pop_mean_f3_5, pop_var_f3_5, fire_5 = fire_f(m_pool_3, sq_ratio*512,256,256, False)
    '''w_s1x1_6, we1x1_6, we3x3_6, gamma_f1_6, beta_f1_6, pop_mean_f1_6, pop_var_f1_6, gamma_f2_6, beta_f2_6, pop_mean_f2_6, pop_var_f2_6, gamma_f3_6, beta_f3_6, pop_mean_f3_6, pop_var_f3_6, fire_6 = fire_f(fire_5,   sq_ratio*512,256,256, True)
    m_pool_4 = max_pool(fire_6)
    #Final-Fire-Module
    w_s1x1_7, we1x1_7, we3x3_7, gamma_f1_7, beta_f1_7, pop_mean_f1_7, pop_var_f1_7, gamma_f2_7, beta_f2_7, pop_mean_f2_7, pop_var_f2_7, gamma_f3_7, beta_f3_7, pop_mean_f3_7, pop_var_f3_7, fire_7 = fire_f(m_pool_4, sq_ratio*1024,512,512, False)'''
    #Convolution-Final
    w2 = weight([1,1,512,num_classes]);
    conv2 = conv(fire_5, w2)
    conv2_bn, gamma_2, beta_2, pop_mean_2, pop_var_2 = bn(conv2, False)
    relu2 = relu(conv2_bn)
    final_pool = avg_pool(relu2)
    out = tf.reshape(final_pool,[-1, num_classes], name='output_node')
    return x,out,[w1, 
                  w_s1x1_1, we1x1_1, we3x3_1, 
                  w_s1x1_2, we1x1_2, we3x3_2, 
                  w_s1x1_3, we1x1_3, we3x3_3, 
                  w_s1x1_4, we1x1_4, we3x3_4, 
                  w_s1x1_5, we1x1_5, we3x3_5,
                  w2, 
                  gamma_1, beta_1, pop_mean_1, pop_var_1,
                  gamma_f1_1, beta_f1_1, pop_mean_f1_1, pop_var_f1_1, 
                  gamma_f2_1, beta_f2_1, pop_mean_f2_1, pop_var_f2_1, 
                  gamma_f3_1, beta_f3_1, pop_mean_f3_1, pop_var_f3_1, 
                  gamma_f1_2, beta_f1_2, pop_mean_f1_2, pop_var_f1_2, 
                  gamma_f2_2, beta_f2_2, pop_mean_f2_2, pop_var_f2_2, 
                  gamma_f3_2, beta_f3_2, pop_mean_f3_2, pop_var_f3_2,
                  gamma_f1_3, beta_f1_3, pop_mean_f1_3, pop_var_f1_3, 
                  gamma_f2_3, beta_f2_3, pop_mean_f2_3, pop_var_f2_3, 
                  gamma_f3_3, beta_f3_3, pop_mean_f3_3, pop_var_f3_3,
                  gamma_f1_4, beta_f1_4, pop_mean_f1_4, pop_var_f1_4, 
                  gamma_f2_4, beta_f2_4, pop_mean_f2_4, pop_var_f2_4, 
                  gamma_f3_4, beta_f3_4, pop_mean_f3_4, pop_var_f3_4,
                  gamma_f1_5, beta_f1_5, pop_mean_f1_5, pop_var_f1_5, 
                  gamma_f2_5, beta_f2_5, pop_mean_f2_5, pop_var_f2_5, 
                  gamma_f3_5, beta_f3_5, pop_mean_f3_5, pop_var_f3_5,
                  gamma_2, beta_2, pop_mean_2, pop_var_2]

#####################  Export For TF Hub ###############################
def export_module(path):
  spec = hub.create_module_spec(build_model)

  with tf.Graph().as_default():
    module = hub.Module(spec)

    with tf.Session() as session:
      module.export(path, session)


def train():
    x,out,remaining = build_model()
    params = remaining
    y = out
    y_ = tf.placeholder(tf.float32, [None, num_classes], name= "y_")

    cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y))
    train_step = tf.train.AdamOptimizer(learn_rate).minimize(cross_entropy)
    correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    #    Training and Retraining

    test_x,test_y = unzip(list(read_test()))
    #print("test_y shape",test_y.shape)
    test_y = test_y.reshape(len(test_y), num_classes)
    sess=tf.Session()
    sess.run(tf.global_variables_initializer())
    batch_id = latest_weight

    while batch_id < train_steps:

        train_xs, train_ys = unzipTrain(read_train_1(), read_train_2(), read_train_3(), read_train_4())
        train_ys = train_ys.reshape(len(train_ys), num_classes)

        _, currEntropy=sess.run([train_step, cross_entropy], feed_dict={x: train_xs, y_: train_ys})

        if batch_id % train_report == 0:
            print("entropy ", currEntropy)
            currAccuracy = accuracy.eval(session=sess, feed_dict={x: train_xs, y_: train_ys})
            print([batch_id, "Train Accuracy: " + str(currAccuracy)])
            with open(reportFile, 'a') as csvfile:
                fields = ['crossEntropy', 'trainAccuracy']
                writer = csv.DictWriter(csvfile, fieldnames=fields)
                # writer.writeheader()
                writer.writerow({'crossEntropy': currEntropy, 'trainAccuracy': str(currAccuracy)})

        if batch_id % eval_report == 0:
            currTestAcc = accuracy.eval(session=sess, feed_dict={x: test_x[:200], y_: test_y[:200]})
            testEntropy = sess.run([cross_entropy], feed_dict={x: test_x[:200], y_: test_y[:200]})
            print([batch_id, "Test Accuracy: " + str(currTestAcc)])
            with open(testAccFile, 'a') as csvfile:
                fields = ['testAcc', 'testEntropy']
                writer = csv.DictWriter(csvfile, fieldnames=fields)
                # writer.writeheader()
                writer.writerow({'testAcc': currTestAcc, 'testEntropy': testEntropy[0]})

        if batch_id % checkpoint_steps == 0:
            last_weights = [p.eval(session=sess) for p in params]
            np.savez('Weights_Numpy/Weights_{}'.format(batch_id), last_weights)
            minimal_graph = convert_variables_to_constants(sess, sess.graph_def, ['output_node'])
            tf.train.write_graph(minimal_graph, 'Weights_ProtoBuf', 'Classify_{}.pb'.format(batch_id), as_text=False)

        batch_id = batch_id + 1

train()
    